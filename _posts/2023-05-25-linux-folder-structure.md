---
layout: post
title:  "Linux Folder Structure"
date:   2023-05-25 08:50:00 -0500
categories: linux 
---

I'm working in a Linux Embedded development, that forces me to learn a lot in the deep of Linux.

The point of today "Where should I put socket files and databases?"

Reading a little I've found this link, it's a complete guide about the Linux Folder structure

[Filesystem Hierarchy Standard](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html)

At the moment I'll be using the `/var` folder for my purpose. I invite to find out the reason ;)

That's it for now.
